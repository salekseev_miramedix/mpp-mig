import json
import uuid

import openpyxl
import easygui

str_metro_station_bundle = """
{
    "resourceType": "Bundle",
    "type": "transaction",
    "entry": []
}
"""
json_metro_station_bundle = json.loads(str_metro_station_bundle)

filename= easygui.fileopenbox()
book = openpyxl.open(filename, read_only=True)
choice_sheet = easygui.choicebox("Выберите лист", "Лист", book.sheetnames)

sheet = book[choice_sheet]

for row in range(2, sheet.max_row + 1):
    id = sheet[row][0].value
    name = sheet[row][1].value
    color = sheet[row][2].value
    latitude = sheet[row][3].value
    longitude = sheet[row][4].value

    str_metro_station= ("""{
    "resourceType": "MetroStation",
    "id": "%s",
    "name": "%s",
    "color": "%s",
    "position": {
        "latitude": %s,
        "longitude": %s
    }
}""" % (id,name,color,latitude,longitude))

    str_entry = ("""
        {
            "resource": %s,
            "request": {
                "method": "POST",
                "url": "MetroStation"
            }
        }
    """ % (str_metro_station))

    json_entry = json.loads(str_entry)
    json_metro_station_bundle['entry'].append(json_entry)

with open('bundle_metro_station.json', 'w') as file:
    json.dump(json_metro_station_bundle, file, indent=3, ensure_ascii=False)

print('ГОТОВО!')