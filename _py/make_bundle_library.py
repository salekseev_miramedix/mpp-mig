import json
from os import sep
import uuid

import openpyxl
import easygui



filename= easygui.fileopenbox()
book = openpyxl.open(filename, read_only=True)

sheet = book['library']

str_librarys_bundle = """
{
    "resourceType": "Bundle",
    "type": "transaction",
    "entry": []
}
"""
json_librarys_bundle = json.loads(str_librarys_bundle)


for row in range(3, 9):
    id = uuid.uuid4()
    title = sheet[row][1].value
    content = sheet[row][2].value
    preview = sheet[row][3].value
    topic_displays = str(sheet[row][4].value).strip()
    topic_codes = str(sheet[row][5].value).strip().replace(' ', '-').lower()
    disease_codes = sheet[row][6].value
    
    str_library = ("""
        {
            "resourceType": "Library",
            "id": "%s",
            "status": "active",
            "title": "%s",
            "topic": [],
            "type": {
                "coding": [
                    {
                        "code": "asset-collection",
                        "system": "http://terminology.hl7.org/CodeSystem/library-type"
                    }
                ]
            },
            "relatedArtifact": [
                {
                    "url": "%s",
                    "type": "documentation",
                    "label": "preview image"
                }
            ],
            "content": [
                {
                    "url": "%s",
                    "contentType": "text/html"
                }
            ],
            "useContext": []
                
        }
    """ % (id, title, preview, content))
    
    json_library = json.loads(str_library)

    
    for index, value in enumerate(str(disease_codes).split(sep=';')):
        if value == None:
            break
        else:
            disease_code = value
            str_context = ("""
                {
                    "code": {
                        "code": "focus",
                        "system": "http://terminology.hl7.org/CodeSystem/usage-context-type"
                    },
                    "value": {
                        "CodeableConcept": {
                            "coding": [
                                {
                                    "code": "%s",
                                    "system": "urn:1.2.643.5.1.13.13.11.1005"
                                }
                            ]
                        }
                    }
                }
            """ % (disease_code))
            json_context = json.loads(str_context)
            json_library['useContext'].append(json_context)
        

    for index, value in enumerate(str(topic_codes).split(sep=';')):
        if value == None:
            break
        else:
            topic_code = value
            topic_display = str(topic_displays).split(sep=';')[index]
            str_topic = ("""
                {
                        "text": "%s",
                        "coding": [
                            {
                                "code": "%s",
                                "system": "http://miramedix.ru/fhir/CodeSystem/library-topic",
                                "display": "%s",
                                "version": "1.0"
                            }
                        ]
                    }
            """ % (topic_displays, topic_codes, topic_displays))
            json_topic = json.loads(str_topic)
            json_library['topic'].append(json_topic)


    str_entry = ("""
        {
            "request": {
                "method": "POST",
                "url": "Library"
            }
        }
    """)

    json_entry = json.loads(str_entry)
    json_entry['resource'] = json_library
    json_librarys_bundle['entry'].append(json_entry)

print(json_librarys_bundle)

with open('bundle_library_mig.json', 'w') as file:
    json.dump(json_librarys_bundle, file, indent=3, ensure_ascii=False)


print('ГОТОВО!')