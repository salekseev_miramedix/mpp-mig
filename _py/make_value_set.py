import json
import uuid

import openpyxl
import easygui


valueset_id = 'mig-physician-specialties'
valueset_title = 'Специальности врачей МИГ'
valueset_url = f'http://miramedix.ru/fhir/ValueSet/{valueset_id}'

str_valueset = ("""
    {
        "experimental": false,
        "id": "%s",
        "resourceType": "ValueSet",
        "url": "%s",
        "title": "%s",
        "status": "active",
        "compose": {
            "include": [
                {
                    "concept": []
                }
            ]
        }
    }
""" % (valueset_id, valueset_url, valueset_title))
json_valueset = json.loads(str_valueset)

filename= easygui.fileopenbox()
book = openpyxl.open(filename, read_only=True)
choice_sheet = easygui.choicebox("Выберите лист", "Лист", book.sheetnames)

sheet = book[choice_sheet]
for row in range(2, sheet.max_row + 1):
    concept_code = sheet[row][0].value
    concept_display = sheet[row][1].value
    concept_logo = sheet[row][2].value
    str_concept = ("""
        {
            "code": "%s",
            "display": "%s",
            "extension": [
                {
                    "url": "http://mmdx.ru/fhir/StructureDefinition/logo",
                    "valueUrl": "%s"
                }
            ]
        }
    """ % (concept_code, concept_display, concept_logo))

    json_concept = json.loads(str_concept)
    json_valueset['compose']['include'][0]['concept'].append(json_concept)

with open(f'valueset_{valueset_id}.json', 'w') as file:
    json.dump(json_valueset, file, indent=3, ensure_ascii=True)
print(json_valueset)
print('ГОТОВО!')