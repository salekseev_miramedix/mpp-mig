import json
import uuid

import openpyxl
import easygui

str_practitioners_bundle = """
{
    "resourceType": "Bundle",
    "type": "transaction",
    "entry": []
}
"""
json_practitioners_bundle = json.loads(str_practitioners_bundle)

filename= easygui.fileopenbox()
book = openpyxl.open(filename, read_only=True)
choice_sheet = easygui.choicebox("Выберите лист", "Лист", book.sheetnames)

sheet = book[choice_sheet]

for row in range(3, sheet.max_row + 1):
    practitioner_id = sheet[row][0].value
    name_text = sheet[row][1].value
    photo_url = sheet[row][2].value
    experience = sheet[row][3].value
    physician_category = sheet[row][4].value
    physician_category_code = sheet[row][5].value
    academic_degree = sheet[row][6].value
    academic_degree_code = sheet[row][7].value
    academic_rank = sheet[row][8].value

    str_practitioner = ("""{
        "resourceType": "Practitioner",
        "id": "%s",
        "active": true,
        "name": [
            {
                "text": "%s"
            }
        ],
        "qualification": [
            {
                "code": {
                    "text": "%s",
                    "coding": [
                        {
                            "system": "urn:oid:1.2.643.5.1.13.13.11.1494:1.2",
                            "code": "%s"
                        }
                    ]
                }
            },
            {
                "code": {
                    "text": "%s",
                    "coding": [
                        {
                            "system": "urn:oid:1.2.643.5.1.13.13.11.1067:3.1",
                            "code": "%s"
                        }
                    ]
                }
            },
            {
                "code": {
                    "text": "%s",
                    "coding": [
                        {
                            "system": "http://miramedix.ru/fhir/CodeSystem/academic-rank-or-title"
                        }
                    ]
                }
            }
        ],
        "photo": [
            {
                "url": "%s"
            }
        ],
        "extension": [
            {
                "url": "http://mmdx.ru/fhir/StructureDefinition/practitioner-experience",
                "valuePeriod": {
                    "start": "%s"
                }
            }
        ]
    }""" % (practitioner_id,name_text,physician_category,physician_category_code,academic_degree,academic_degree_code, academic_rank, photo_url,experience))

    str_entry = ("""
        {
            "resource": %s,
            "request": {
                "method": "POST",
                "url": "Practitioner"
            }
        }
    """ % (str_practitioner))

    json_entry = json.loads(str_entry)
    json_practitioners_bundle['entry'].append(json_entry)

with open('bundle_practitioners.json', 'w') as file:
    json.dump(json_practitioners_bundle, file, indent=3, ensure_ascii=False)

print('ГОТОВО!')