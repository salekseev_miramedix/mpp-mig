import json
import uuid

import openpyxl
import easygui

str_locations_bundle = """
{
    "resourceType": "Bundle",
    "type": "transaction",
    "entry": []
}
"""
json_locations_bundle = json.loads(str_locations_bundle)

filename= easygui.fileopenbox()
book = openpyxl.open(filename, read_only=True)
choice_sheet = easygui.choicebox("Выберите лист", "Лист", book.sheetnames)

sheet = book[choice_sheet]

for row in range(3, sheet.max_row + 1):
    location_id = sheet[row][0].value
    organization_id = sheet[row][1].value
    address_text = sheet[row][2].value
    latitude = sheet[row][3].value
    longitude = sheet[row][4].value
    metro_id = sheet[row][5].value

    str_location = ("""{
        "resourceType": "Location",
        "id": "%s",
        "status": "active",
        "mode": "instance",
        "managingOrganization": {
            "resourceType": "Organization",
            "id": "%s"
        },
        "address": {
            "text": "%s"
        },
        "position": {
            "latitude": %s,
            "longitude": %s
        },
        "hoursOfOperation":[
            {
                "daysOfWeek": ["mon", "tue", "wed", "thu", "fri", "sat"],
                "openingTime": "08:00:00",
                "closingTime": "20:00:00",
                "allDay": false
            },
            {
                "daysOfWeek": ["sat"],
                "openingTime": "10:00:00",
                "closingTime": "18:00:00",
                "allDay": false
            }
        ],
        "extension": [
            {
                "url": "http://mmdx.ru/fhir/StructureDefinition/metro-station-id",
                "valueReference": {
                    "id": "%s",
                    "ResourceType": "MetroStation"
                }
            }
        ]
    }""" % (location_id, organization_id, address_text, latitude, longitude, metro_id))

    str_entry = ("""
        {
            "resource": %s,
            "request": {
                "method": "POST",
                "url": "Location"
            }
        }
    """ % (str_location))

    json_entry = json.loads(str_entry)
    json_locations_bundle['entry'].append(json_entry)

with open('bundle_locations.json', 'w') as file:
    json.dump(json_locations_bundle, file, indent=3, ensure_ascii=False)

print('ГОТОВО!')