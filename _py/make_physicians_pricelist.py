import json
import uuid

import openpyxl
import easygui

str_pricelist = """
{
    "resourceType": "PriceList",
    "id": "mig-physicians-pricelist",
    "provider": []
}
"""
json_pricelist = json.loads(str_pricelist)

filename= easygui.fileopenbox()
book = openpyxl.open(filename, read_only=True)
choice_sheet = easygui.choicebox("Выберите лист", "Лист", book.sheetnames)

sheet = book[choice_sheet]

for row in range(1, sheet.max_row + 1, 2):
    practitioner_role_id = sheet[row][0].value
    str_provider = ("""{
        "reference": {
            "id": "%s",
            "resourceType": "PractitionerRole"
        },
        "service": []
    }""" % (practitioner_role_id))
    json_provider = json.loads(str_provider)
    for i in range(2):
        service_id = sheet[row + i][1].value
        price_low = sheet[row + i][2].value

        str_service = ("""{
            "reference": {
                "id": "%s",
                "resourceType": "HealthcareService"
            },
            "active": true,
            "price": {
                "low": {
                    "value": %s,
                    "currency": "RUB"
                }
            }
        }""" % (service_id, price_low))

        json_service = json.loads(str_service)
        json_provider['service'].append(json_service)
    json_pricelist['provider'].append(json_provider)

with open('mig_physicians_pricelist.json', 'w') as file:
    json.dump(json_pricelist, file, indent=3, ensure_ascii=False)

print('ГОТОВО!')