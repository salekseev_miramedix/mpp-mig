import json
from os import sep
import uuid

import openpyxl
import easygui

str_practitioner_roles_bundle = """
{
    "resourceType": "Bundle",
    "type": "transaction",
    "entry": []
}
"""
json_practitioner_roles_bundle = json.loads(str_practitioner_roles_bundle)

filename= easygui.fileopenbox()
book = openpyxl.open(filename, read_only=True)
choice_sheet = easygui.choicebox("Выберите лист", "Лист", book.sheetnames)

sheet = book[choice_sheet]

for row in range(3, sheet.max_row + 1):
    practitioner_role_id = sheet[row][0].value
    practitioner_id = sheet[row][1].value
    organization_id = sheet[row][2].value
    location_id = sheet[row][3].value
    code_text = sheet[row][4].value
    specialty_code = sheet[row][5].value
    specialty_display = sheet[row][6].value

    str_practitioner_role = ("""
        {
            "resourceType": "PractitionerRole",
            "id": "%s",
            "active": true,
            "practitioner": {
                "resourceType": "Practitioner",
                "id": "%s"
            },
            "organization": {
                "resourceType": "Organization",
                "id": "%s"
            },
            "location": [],
            "code": [
                {
                    "text": "%s"
                }
            ],
            "specialty": []
        }
    """ % (practitioner_role_id, practitioner_id, organization_id, code_text))
    
    json_practitioner_role = json.loads(str_practitioner_role)

    for id in str(location_id).split(sep=';'):
        str_location = ("""
            {
                "resourceType": "Location",
                "id": "%s"
            }
        """ % (id))
        json_location = json.loads(str_location)
        json_practitioner_role['location'].append(json_location)

    for index, value in enumerate(str(specialty_code).split(sep=';')):
        code = value
        display = str(specialty_display).split(sep=';')[index]
        str_specialty = ("""
            {
                "coding": [
                    {
                        "code": "%s",
                        "system": "urn:oid:1.2.643.5.1.13.2.1.1.208:1.0",
                        "display": "%s"
                    }
                ]
            }
        """ % (code, display))
        json_specialty = json.loads(str_specialty)
        json_practitioner_role['specialty'].append(json_specialty)

    str_entry = ("""
        {
            "request": {
                "method": "POST",
                "url": "PractitionerRole"
            }
        }
    """)

    json_entry = json.loads(str_entry)
    json_entry['resource'] = json_practitioner_role
    json_practitioner_roles_bundle['entry'].append(json_entry)

print(json_practitioner_roles_bundle)

with open('bundle_practitioner_roles.json', 'w') as file:
    json.dump(json_practitioner_roles_bundle, file, indent=3, ensure_ascii=False)


print('ГОТОВО!')