import requests
import easygui

if __name__ == "__main__":
    filename = easygui.fileopenbox()
    dirname = easygui.diropenbox()
    with open(filename) as list:
        for line in list.readlines():
            try:
                url = line.strip()
                content = requests.get(url).content
                file = url.split('/')[-1]
            except:
                print('Не удалось скачать')
            else:
                photo = open(f'{dirname}/{file}', 'wb')
                photo.write(content)
                photo.close()
                print(f'{file} скачан!')
    