import json
from os import sep
import uuid

import openpyxl
import easygui

str_services_bundle = """
{
    "resourceType": "Bundle",
    "type": "transaction",
    "entry": []
}
"""
json_services_bundle = json.loads(str_services_bundle)

filename= easygui.fileopenbox()
book = openpyxl.open(filename, read_only=True)
choice_sheet = easygui.choicebox("Выберите лист", "Лист", book.sheetnames)

sheet = book[choice_sheet]

for row in range(2, sheet.max_row + 1):
    service_id = sheet[row][0].value
    service_name = sheet[row][1].value
    comment = sheet[row][2].value
    details = sheet[row][3].value
    specialty_code = sheet[row][4].value
    specialty_display = sheet[row][5].value

    str_service = ("""
        {
            "resourceType": "HealthcareService",
            "id": "%s",
            "name": "%s",
            "active": true,
            "specialty": [],
            "comment": "%s",
            "category": [
                {
                    "coding": [
                        {
                        "code": "external",
                        "system": "http://miramedix.ru/fhir/CodeSystem/healthcareservice-category",
                        "display": "Внешняя"
                        }
                    ]
                }
            ],
            "extraDetails": "%s"
        }
    """ % (service_id, service_name, comment, details))
    
    json_service = json.loads(str_service)

    for index, value in enumerate(str(specialty_code).split(sep=';')):
        code = value
        display = str(specialty_display).split(sep=';')[index]
        str_specialty = ("""
            {
                "coding": [
                    {
                        "code": "%s",
                        "system": "urn:oid:1.2.643.5.1.13.2.1.1.208:1.0",
                        "display": "%s"
                    }
                ]
            }
        """ % (code, display))
        json_specialty = json.loads(str_specialty)
        json_service['specialty'].append(json_specialty)

    str_entry = ("""
        {
            "request": {
                "method": "POST",
                "url": "HealthcareService"
            }
        }
    """)

    json_entry = json.loads(str_entry)
    json_entry['resource'] = json_service
    json_services_bundle['entry'].append(json_entry)

print(json_services_bundle)

with open('bundle_services.json', 'w') as file:
    json.dump(json_services_bundle, file, indent=3, ensure_ascii=False)


print('ГОТОВО!')